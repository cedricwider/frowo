'use strict';

describe('Controller: IntroCtrl', function () {

  // load the controller's module
  beforeEach(module('woopApp'));

  var IntroCtrl, scope, location;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();

    location = {
      href: null,

      path: function(href) {
        this.href = href;
      }
    };

    IntroCtrl = $controller('IntroCtrl', {
      $scope: scope,
      $location: location
    });
  }));


  it('should attach date to the scope', function () {
    expect(scope.today).toBeDefined();
  });

  it('should attach methods for signup/login to the scope', function () {
      expect(scope.loginClicked).toBeDefined();
      expect(scope.signupClicked).toBeDefined();
    }
  );
});
