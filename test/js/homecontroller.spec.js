'use strict';

describe('Controller: HomeCtrl', function () {

  // load controller's module
  beforeEach(module('woopApp'));

  var HomeCtrl, scope;

  beforeEach(inject(function($controller, $rootScope) {
    var meService = {me: 'user'};
    scope = $rootScope.$new();

    // create controller
    HomeCtrl = $controller('HomeCtrl', {
      $scope: scope,
      MeService: meService
    });
  }));


  it('attaches user from meService to scope', function () {
    expect(scope.user).toBe('user');
  });

});