'use strict';

describe('Controller: LoginCtrl', function () {

  // load the controller's module
  beforeEach(module('woopApp'));

  var LoginCtrl, scope, location, httpBackend, rootScope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope, $httpBackend) {
    scope = $rootScope.$new();
    rootScope = $rootScope;

    // mock $location object
    location = {
      href: null,

      path: function(_path) {
        this.href = _path;
      }
    };
    httpBackend = $httpBackend;
    httpBackend.when('GET', '/api/users/me').respond(401, 'unauthorized'); // irrelevant for this test

    LoginCtrl = $controller('LoginCtrl', {
      $scope: scope,
      $location: location,
      $rootScope: rootScope
    });
  }));

  afterEach(function () {
    httpBackend.verifyNoOutstandingExpectation();
    httpBackend.verifyNoOutstandingRequest();
    httpBackend.resetExpectations();
  });

  it('should attach email and password to the scope', function () {
    expect(scope.email).toBe(null);
    expect(scope.password).toBe(null);
    httpBackend.flush();
  });

  it('should attach a login function to the scope', function () {
    expect(scope.doLogin).toBeDefined();
    httpBackend.flush();
  });

  it('should redirect to /home after successful login', function () {
    // given
    httpBackend.when('POST', '/api/login').respond(200, '"{message: "OK"}"');
    scope.email = 'test@test.com';
    scope.password = 'password';

    // when
    scope.doLogin();
    httpBackend.flush();

    // then
    expect(scope.error).toBeUndefined();
    expect(location.href).toBe('/home');
  });

  it('should attach an error object to the scope if login is not successful', function () {
    // given
    var message = {
      message: 'Wrong password'
    };
    httpBackend.when('POST', '/api/login').respond(403, message);
    scope.email = 'test@test.com';
    scope.password = 'password';

    // when
    scope.doLogin();
    httpBackend.flush();

    // then
    expect(location.href).toBe(null);
    expect(scope.error).toBe('Wrong password');
  });

  it('should attach AuthToken to root scope after successful login', function () {
    // given
    var message = {
      message: 'Success',
      authToken: '123'
    };
    httpBackend.when('POST', '/api/login').respond(200, message);
    scope.email = 'test@test.com';
    scope.password = 'password';

    // when
    scope.doLogin();
    httpBackend.flush();

    // then
    expect(rootScope.token).toBe('123');
  });

  it ('should attach method for cancelling to scope', function () {
    expect(scope.navigateBack).toBeDefined();
    httpBackend.flush();
  });
});
