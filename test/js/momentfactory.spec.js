'use strict';

describe('Factory: MomentFactory', function () {
  // load controller's module
  beforeEach(module('woopApp'));

  var factory, httpBackend;

  beforeEach(inject(function(MomentFactory, $httpBackend, MeService){
    factory = MomentFactory;
    httpBackend = $httpBackend;
    httpBackend.when('GET', '/api/users/me').respond(200, {_id: '123', username: 'test@test.com'});
    MeService.loadMe();
  }));

  afterEach(function () {
    httpBackend.verifyNoOutstandingExpectation();
    httpBackend.verifyNoOutstandingRequest();
    httpBackend.resetExpectations();
  });

  it('loads moments as list', function() {
    // given
    httpBackend.expect('GET', '/api/users/moments').respond(200, [{_id: 1, title: 'event1'}, {_id: 2, title: 'event2'}]);

    // when
    factory.loadMoments(function(moments) {
      // then
      expect(moments.length).toBe(2);
    }, function(response) {
      console.log(response);
    });
    httpBackend.flush();
  });

  it('loads a single moment', function () {
    httpBackend.flush();
  });

  it('saves a new moment with POST', function() {
    httpBackend.flush();
  });

  it('saves an existing moment with PUT', function() {
    httpBackend.flush();
  });

});