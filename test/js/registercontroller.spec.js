'use strict';

describe('Controller: RegisterCtrl', function () {

  var RegisterCtrl, scope, httpBackend, token, broadcastData, location, rootScope;

  beforeEach(module('woopApp'));

  beforeEach(inject(function ($controller, $rootScope, $httpBackend) {
    scope = $rootScope.$new();
    httpBackend = $httpBackend;
    httpBackend.when('GET', '/api/users/me').respond(401, '"{"message":"Unauthorized"}"');

    var tokenService = {
      saveToken: function (tokenData) {
        token = tokenData;
      }
    };

    rootScope = {
      $broadcast: function(message, data) {
        broadcastData = data;
      }
    };

    location = {
      href: null,
      path: function(loc) {
        this.href = loc;
      }
    };

    RegisterCtrl = $controller('RegisterCtrl', {
      $scope: scope,
      $rootScope: rootScope,
      TokenService: tokenService,
      $location: location
    });
  }));

  afterEach(function () {
    httpBackend.verifyNoOutstandingExpectation();
    httpBackend.verifyNoOutstandingRequest();
  });

  it('attaches an empty request object to scope', function () {
    expect(scope.request).toEqual({});
    httpBackend.flush();
  });

  it('attaches a register function', function () {
    expect(scope.register).toBeDefined();
    httpBackend.flush();
  });

  it('resets request object and error message when calling reset', function () {
    // given
    expect(scope.reset).toBeDefined();
    scope.request = {email: 'test@test.com', password: 'Passw0rD', pwdRepeat: 'Passw0rD'};
    scope.error = 'error';

    // when
    scope.reset();

    // then
    expect(scope.request).toEqual({});
    expect(scope.error).toBeUndefined();

    httpBackend.flush();
  });

  it('submits a registration request when calling register()', function () {
    // given
    httpBackend.expect('POST', '/api/register').respond(200, '{"authToken": "123"}');

    // when
    scope.register({email: 'test@test.com', password: 'Passw0rD', pwdRepeat: 'Passw0rD'});
    httpBackend.flush();
  });

  it('sets authtoken upon successful reponse', function () {
    // given
    httpBackend.expect('POST', '/api/register').respond(200, '{"authToken": "123"}');

    // when
    scope.register({email: 'test@test.com', password: 'Passw0rD', pwdRepeat: 'Passw0rD'});
    httpBackend.flush();

    // then
    expect(token).toEqual('123');
  });

  it('adds error message to scope upon failed registration request', function () {
    // given
    httpBackend.expect('POST', '/api/register').respond(400, '{"error":"Username already exists!", "detail":{"field":"field", "message":"message"}}');

    // when
    scope.register({email: 'test@test.com', password: 'Passw0rD', pwdRepeat: 'Passw0rD'});
    httpBackend.flush();

    expect(scope.error.error).toEqual('Username already exists!');
  });

  it('broadcasts login event on rootScope', function () {
    // given
    httpBackend.expect('POST', '/api/register').respond(200, '{"authToken": "123"}');

    // when
    scope.register({email: 'test@test.com', password: 'Passw0rD', pwdRepeat: 'Passw0rD'});
    httpBackend.flush();

    // then
    expect(broadcastData).toEqual({success: true});

  });

  it('redirects to /home after successful registration', function () {
    // given
    httpBackend.expect('POST', '/api/register').respond(200, '{"authToken": "123"}');

    // when
    scope.register({email: 'test@test.com', password: 'Passw0rD', pwdRepeat: 'Passw0rD'});
    httpBackend.flush();

    // then
    expect(location.href).toEqual('/home');

  });

  it('informs which field the backend thought was invalid', function () {
    // given
    httpBackend.expect('POST', '/api/register').respond(400, '{"error": "invalid request!", "detail": {"field": "username", "message": "username is required"}}');

    // when
    scope.register({email: 'test@test.com', password: 'Passw0rD', pwdRepeat: 'Passw0rD'});
    httpBackend.flush();

    // then
    expect(scope.error.detail.field).toEqual('username');
    expect(scope.error.detail.message).toEqual('username is required');

  });
});