'use strict';

describe('Controller: AuthRespCtrl', function () {

  // load the controller's module
  beforeEach(module('woopApp'));

  var AuthRespCtrl, scope, rootScope, location, tokenService;
  var authSuccess = false;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    rootScope = $rootScope;
    scope = $rootScope.$new();
    scope.$on('login', function(event, data) {
      authSuccess = data.success;
    });

    var routeParams = {
      token: 'mock-token'
    };

    // mock $location object
    location = {
      href: null,

      path: function(_path) {
        this.href = _path;
      }
    };

    // mock UserFactory
    var meService = {
      tkn: 'token',
      loadMeAnd: function (callback) {
        callback('user');
      },
      token: function (token) {
        this.tkn = token;
      }

    };

    tokenService = {
      saveToken: function(token) {
        this.token = token;
      }
    };


    AuthRespCtrl = $controller('AuthRespCtrl', {
      $scope: scope,
      $routeParams: routeParams,
      $rootScope: rootScope,
      $location: location,
      MeService: meService,
      TokenService: tokenService
    });
  }));



  it('should attach token to root scope', function () {
    expect(tokenService.token).toBe('mock-token');
  });

  it('should attach user to root scope', function () {
    expect(rootScope.user).toBeDefined();
  });

  it('should redirect to /home route', function () {
    expect(location.href).toBe('/home');
  });

  it('should fire login-event', function () {
    expect(authSuccess).toBe(true);
  });

});