'use strict';

describe('Controller: TimelineCtrl', function () {

  // load controller's module
  beforeEach(module('woopApp'));

  var TimelineCtrl, scope, momentFactory, dialog, location, controller;

  function createController($controller, scp, lctn, mmntfctr, dlg) {
    // create controller
    TimelineCtrl = $controller('TimelineCtrl', {
      $scope: scp,
      $location: lctn,
      MomentFactory: mmntfctr,
      $modal: dlg
    });
  }

  beforeEach(inject(function ($controller, $rootScope) {
    controller = $controller;
    scope = $rootScope.$new();

    momentFactory = {
      loaded: false,
      deleted: false,

      loadMoments: function(callback) {
        this.loaded = true;
        callback([]);
      },
      deleteMoment: function(moment) {
        this.deleted = true;
        moment = null;
      }
    };

    location = {
      href: null,
      path: function(loc) {
        this.href = loc;
      }
    };

    dialog = {
      config: undefined,
      open: function(options) {
        this.config = options;
        return {result: {then: function(){}}};
      }
    };

    createController($controller, scope, location, momentFactory, dialog);
  }));

  it('attaches moments to scope', function() {
    expect(scope.moments).toBeDefined();
  });

  it('attaches a load indicator to scope', function() {
    expect(scope.loading).toBeDefined();
  });

  it('loads moments', function () {
    expect(momentFactory.loaded).toBe(true);
  });

  it('offers a method to create an event', function() {
    expect(scope.createEvent).toBeDefined();
  });

  it('redirects to /moment/new when createEvent is called', function () {
    scope.createEvent();
    expect(location.href).toEqual('/moment/new');
  });

  it('offers possibility to delete moments', function() {
    expect(scope.deleteMoment).toBeDefined();
  });

  it('deletes moments using factory', function () {
    scope.deleteMoment({});
    expect(momentFactory.deleted).toBe(true);
  });

  it('adds error to scope when timeline couldnt be loaded', function () {
    // given
    var momentFactory = {
      loadMoments: function (_, errorCallback) {
            errorCallback({data: {message: 'test error', details: {field1: 'field1'}}});
          }
    };

    // when
    createController(controller, scope, location, momentFactory, dialog);

    // then
    expect(scope.error.message).toEqual('test error');
    expect(scope.error.details[0]).toEqual('field1: field1');
  });

  it('has the possibility to expose a moment', function () {
    // given
    var momentOne = {id: 1};
    var momentTwo = {id: 2};
    scope.moments = [momentOne, momentTwo];

    // when
    scope.expose(momentOne);

    // then
    expect(scope.exposed).toEqual(momentOne);
  });

  it('has the possiblity to un-expose a moment', function () {
    // given
    scope.exposed = {id: 1};

    // when
    scope.unexpose();

    // then
    expect(scope.exposed).toBe(undefined);
  });

  it('opens moment in a dialog', function () {
    // when
    scope.expose({});

    // then
    expect(dialog.config).toBeDefined();
  });
});