'use strict';

describe('Controller: NavigationCtrl', function () {

  beforeEach(module('woopApp'));

  var NavigationCtrl, scope, location;

  // initialize controller and mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();

    // mock $location object
    location = {
      href: null,

      path: function(_path) {
        this.href = _path;
      }
    };

    NavigationCtrl = $controller('NavigationCtrl', {
      $scope: scope,
      $location: location
    });
  }));


  it('attaches applications name to scope', function () {
    expect(scope.appName).toBe('Woop!');
  });

  it('defines navigation items', function () {
    expect(scope.navItems).toBeDefined();
  });

  it('offers possibility to navigate to a certain location', function () {
    expect(scope.navigate).toBeDefined();

    scope.navigate('/doener');

    expect(location.href).toBe('/doener');
  });

  it('adds more navitems after login event', function () {
    scope.$broadcast('login', {success: true});

    expect(scope.navItems.length).toBeGreaterThan(1);
  });

  it('offers a link to timeline after login event', function () {
    scope.$broadcast('login', {success: true});

    expect(scope.navItems).toContain({url: '/#/timeline', text: 'Timeline'});
  });
});