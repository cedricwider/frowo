'use strict';

describe('Controller: MomentCtrl', function () {

  // load controller's module
  beforeEach(module('woopApp'));

  var MomentCtrl, scope, routeParams, controller, momentFactory;

  beforeEach(inject(function ($controller, $rootScope) {
    controller = $controller;
    scope = $rootScope.$new();
    // create controller
    MomentCtrl = $controller('MomentCtrl',
    {
      $scope: scope
    });
  }));

  it('detects when id was "new" and creates an empty moment for it', function() {
    // given
    routeParams = {id: 'new'};

    // when
    controller('MomentCtrl', {
      $scope: scope,
      $routeParams: routeParams
    });

    // then
    expect(scope.moment).toEqual({});
  });

  it('reverts changes', function() {
    // given
    scope.moment = {test: 'value'};

    // when
    scope.clearForm();

    // then
    expect(scope.moment).toEqual({});

  });

  it('submits changes', function() {
    // given
    momentFactory = {
      moment: null,
      save: function(mmnt) {
        this.moment = mmnt;
      }
    };

    controller('MomentCtrl', {
      $scope: scope,
      MomentFactory: momentFactory
    });

    // when
    scope.saveMoment({test: 'value'});

    // then
    expect(momentFactory.moment.test).toEqual('value');

  });

  it('adds loaded moment to scope', function() {
    // given
    momentFactory = {
      loadMoment: function(id, success) {
        success({test: 'value'});
      }
    };

    routeParams = {id: '123'};

    // when
    controller('MomentCtrl', {
      $scope: scope,
      MomentFactory: momentFactory,
      $routeParams: routeParams
    });

    // then
    expect(scope.moment).toEqual({test: 'value'});
  });

  it('adds required field to newly created moment', function () {
    // given
    momentFactory = {
      moment: null,
      save: function(mmnt) {
        this.moment = mmnt;
      }
    };

    controller('MomentCtrl', {
      $scope: scope,
      MomentFactory: momentFactory
    });

    // when
    scope.saveMoment({test: 'value'});

    // then
    var moment = momentFactory.moment;
    expect(moment.provider).toBeDefined();
  });

  it('has a function to set the mood', function() {
    expect(scope.setMood).toBeDefined();
  });

  it('adds mood to newly created moment', function() {
    // given
    var mood = 8;

    // when
    scope.rating = mood; // <- this is done via <rating/> directive
    scope.setMood();

    // then
    expect(scope.moment.mood).toEqual(mood);
  });

  it('offers possibility to set location', function () {
    expect(scope.setGeoLocation).toBeDefined();
  });
});