'use strict';

var woopApp = woopApp || {};

woopApp.factory('UserFactory', ['Restangular', function(Restangular) {
  var factory = {};

  factory.findMe = function (callback) {
    Restangular.one('users', 'me').get({}, {authtoken: factory.authToken}).then(callback, function(response) {console.log(response);});
  };

  factory.token = function(token) {
    factory.authToken = token;
  };

  return factory;
}]);