'use strict';

var woopApp = woopApp || {};

woopApp.factory('MomentFactory', ['Restangular', 'MeService', 'TokenService',
function (restangular, meService, tokenService) {

  // default configuration for all calls
  restangular.setDefaultHeaders({authtoken: tokenService.getToken()});

  var factory = {
    loadMoments: function(success, error) {
      if (meService.me) {
        meService.me.getList('moments').then(success, error);
      } else {
        meService.loadMeAnd(function (me) {
          me.getList('moments').then(success, error);
        });
      }
    },

    loadMoment: function(id, success, error) {
      meService.me.one('moments', id).get().then(success, error);
    },

    save: function(moment, success, error) {
      moment.updated_at = new Date();
      if (moment._id) { // existing object
        moment.save().then(success, error);
      } else {
        moment.created_at = new Date();
        meService.me.post('moments', moment).then(success, error);
      }
    },

    deleteMoment: function(moment, success, error) {
      moment.remove().then(success, error);
    }
  };

  return factory;

}]);