'use strict';

var woopApp = angular.module('woopApp', ['ngRoute', 'restangular', 'LocalStorageModule', 'ui.bootstrap']);

woopApp.config(['$routeProvider', function($routeProvider){
  $routeProvider.
    when('/', {
        templateUrl: '/partials/intro.html',
        controller: 'IntroCtrl'
    })
    .when('/login', {
        templateUrl: '/partials/login.html',
        controller: 'LoginCtrl'
    })
    .when('/authresp/:token', {
      templateUrl: '/partials/pleasewait.html',
      controller: 'AuthRespCtrl'
    })
    .when('/home', {
      templateUrl: '/partials/home.html',
      controller: 'HomeCtrl'
    })
    .when('/signup', {
      templateUrl: '/partials/signup.html',
      controller: 'RegisterCtrl'
    })
    .when('/timeline', {
      templateUrl: '/partials/timeline.html',
      controller: 'TimelineCtrl'
    })
    .when('/moment/:id', {
      templateUrl: '/partials/moment.html',
      controller: 'MomentCtrl'
    })

    .otherwise(
        {redirectTo: '/'}
    );
}]);

woopApp.config(function(RestangularProvider){
  RestangularProvider.setBaseUrl('/api');
  RestangularProvider.setRestangularFields({
      id: '_id.$oid'
  });
});

woopApp.config(function (localStorageServiceProvider) {
  localStorageServiceProvider.setPrefix('woop');
});