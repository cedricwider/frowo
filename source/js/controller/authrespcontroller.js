'use strict';

var woopApp = woopApp || {};

woopApp.controller('AuthRespCtrl', ['$rootScope', '$scope', '$location', '$routeParams', 'MeService', 'TokenService', function(rootScope, scope, location, routeParams, meService, tokenService){
  function proceedWithUser(user) {
    rootScope.user = user;
    rootScope.$broadcast('login', {success: true});
    location.path('/home');
  }

  tokenService.saveToken(routeParams.token);
  meService.loadMeAnd(proceedWithUser);
}]);