'use strict';

var woopApp = woopApp || {};

woopApp.controller('RegisterCtrl', ['$scope', '$rootScope', '$http', 'TokenService', '$location',
  function (scope, rootScope, http, tokenService, location) {
    scope.request = {};

    scope.register = function(request) {
      http.post('/api/register', request)
        .success(success)
        .error(error);
    };

    scope.reset = function() {
      scope.request = {};
      scope.error = undefined;
      location.path('/');
    };

    function success(data) {
      tokenService.saveToken(data.authToken);
      rootScope.$broadcast('login', {success: true});
      location.path('/home');
    }

    function error(data, status) {
      console.log('error while login: ' + status + ' - ' + JSON.stringify(data));
      scope.error = data;
    }
  }
]);