'use strict';

var woopApp = woopApp || {};

woopApp.controller('IntroCtrl', ['$scope', '$location', function (scope, location) {
  scope.today = new Date();

  scope.loginClicked = function () {
    location.path('/login');
  };

  scope.signupClicked = function () {
    location.path('/signup');
  };
}]);