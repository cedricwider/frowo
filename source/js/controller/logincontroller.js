'use strict';

var woopApp = woopApp || {};

woopApp.controller('LoginCtrl', ['$scope', '$rootScope', '$location', '$http', '$window',
  function ($scope, rootScope, location, http, $window) {
    function success(data) {
      rootScope.token = data.authToken;
      location.path('/home');
    }

    function error(data, status) {
      console.log('error while login: ' + status + ' - ' + data.message);
      $scope.error = data.message;
    }

    $scope.email = null;
    $scope.password = null;

    $scope.doLogin = function () {
      console.log('going to post /api/login with ' + $scope.email + ' and ' + $scope.password);
      http.post('/api/login', {username: $scope.email, password: $scope.password})
        .success(success)
        .error(error);
    };

    $scope.navigateBack = function () {
      $window.history.back();
    };
  }
]);