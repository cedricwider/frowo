'use strict';

var woopApp = woopApp || {};

woopApp.controller('NavigationCtrl', ['$scope', '$rootScope', '$location', '$http', 'TokenService', function (scope, rootScope, location, http, tokenService) {
  var navItemsWithLogin = [
    { url: '/#/home', text: 'Home'},
    { url: '/#/timeline', text: 'Timeline'},
    { url: '/#/profile', text: 'Profile'},
    { url: '/#/about', text: 'About'}
  ];
  var navItemsNoLogin = [{
      url: '/#/about',
      text: 'About'
    }];

  scope.appName='Woop!';
  scope.navItems = navItemsNoLogin;
  scope.showLogout = false;

  scope.navigate = function(there) {
    location.path(there);
  };

  scope.logout = function () {
    http.post('/api/logout', null, {headers: {authtoken: tokenService.getToken()}});
    tokenService.destroyToken();
    rootScope.$broadcast('logout');
    return false;
  };

  scope.$on('login', function(event, data) {
    if (data.success) {
      scope.navItems = navItemsWithLogin;
    } else {
      scope.navItems = navItemsNoLogin;
    }
    scope.showLogout = data.success;
  });

  scope.$on('logout', function () {
    scope.showLogout = false;
    scope.navItems = navItemsNoLogin;
  });

}]);