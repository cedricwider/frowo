'use strict';

var woopApp = woopApp || {};

woopApp.controller('MomentCtrl', ['$scope', '$routeParams', 'MomentFactory', 'Utils', '$location', '$window', 'GeoLocationService',
  function (scope, routeParams, momentFactory, util, location, wndw, geoService) {

    scope.maxMood = 10;
    scope.rating = 7;

    scope.clearForm = function() {
      scope.moment = {};
      scope.error = undefined;
      location.path('/timeline');
    };

    scope.setMood = function() {
      if (scope.moment) {
        scope.moment.mood = scope.rating;
      }
    };

    scope.saveMoment = function (moment) {
      moment.provider = 'dairy';
      momentFactory.save(moment,
        function(){
          location.path('/timeline');
        },
        function(response){
          util.handleError(response, scope);
        });
    };

    scope.hoveringOver = function(value) {
      scope.overStar = value;
      scope.percentage = 100 * (value / scope.maxMood);
    };

    scope.setGeoLocation = function() {
      wndw.navigator.geolocation.getCurrentPosition(function(position) {
        scope.$apply(function() {
          scope.moment.location = [position.coords.latitude, position.coords.longitude];
            geoService.getOneAddress(scope.moment.location, function(address) {
              scope.moment.resolved_location = address;
            });
        });
      });
    };

    function init(scope, routeParams, momentFactory, util) {
      if (routeParams.id === 'new' || routeParams.id === undefined) {
        scope.moment = {};
      } else {
        momentFactory.loadMoment(routeParams.id, function (moment) {
          scope.moment = moment;
          scope.rating = moment.mood;
        }, function (response) {
          util.handleError(response, scope);
        });
      }
    }

    init(scope, routeParams, momentFactory, util);
  }
]);