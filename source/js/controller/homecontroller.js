'use strict';

var woopApp = woopApp || {};

woopApp.controller('HomeCtrl', ['$scope', 'MeService', function(scope, meService) {
  function setMe(_user) {
    scope.user = _user;
  }

  scope.user = meService.me || meService.loadMeAnd(setMe);
}]);