'use strict';

var woopApp = woopApp || {};

woopApp.controller('TimelineCtrl', ['$scope', '$location', 'MomentFactory', 'Utils', '$modal',
  function (scope, location, momentFactory, utils, dialog) {

    function loadMoments(factory) {
      factory.loadMoments(
        function (moments) { // success
          scope.loading = false;
          scope.moments = moments;
          scope.empty = scope.moments.length < 1;
        },
        function(response) { // error
          utils.handleError(response, scope);
        }
      );
    }

    scope.loading = true;
    scope.moments = [];
    scope.exposed = undefined;

    scope.createEvent = function() {
      location.path('/moment/new');
    };

    scope.showDetails = function(moment) {
      var url = '/moment/' + moment._id.$oid;
      console.log(url);
      location.path(url);
    };

    scope.deleteMoment = function(moment) {
      momentFactory.deleteMoment(moment,
        function () { // success
          console.log('Successfully deleted moment');
          var idx = scope.moments.indexOf(moment);
          scope.moments.splice(idx, 1);
          scope.empty = scope.moments.length < 1;
        }, function (response) { // error
          utils.handleError(response, scope);
        });
      return false;
    };

    scope.expose = function(moment) {
      scope.exposed = moment;
      var modalInstance = dialog.open({
        templateUrl: '/partials/moment_dialog.html',
        controller: modalMomentCtrl,
        resolve: {
          event: function() {
            return scope.exposed;
          }
        }
      });

      modalInstance.result.then(function (event) {
        scope.showDetails(event);
      });
    };

    scope.unexpose = function() {
      scope.exposed = undefined;
    };

    /**
     * Controller for modal view.
     * @param $scope auto-injected via AngularJS
     * @param $modalInstance The instance of the modal view
     * @param event The event to display. (Injected via calling controller)
     */
    var modalMomentCtrl = function($scope, $modalInstance, event) {
      $scope.moment = event;

      $scope.dismiss = function() {
        $modalInstance.dismiss();
      };

      $scope.showDetails = function() {
        $modalInstance.close(event);
      };
    };
    loadMoments(momentFactory);
  }
]);