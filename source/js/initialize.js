'use strict';

var woopApp = woopApp || {};

woopApp.run(['TokenService', function(tokenService) {
  tokenService.restoreToken();
}]);

woopApp.run(['MeService', '$location', '$rootScope', function(userService, location, rootScope) {
  userService.loadMeAnd(function() {
    rootScope.$broadcast('login', {success: true});
    location.path('/home');
  });
}]);