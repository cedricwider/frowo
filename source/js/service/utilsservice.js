'use strict';

var woopApp = woopApp || {};

woopApp.service('Utils', function() {

  var service = {

    // parses standard error response from backend
    // and adds standard fields to scope, so it can
    // be displayed the same on all pages that deal
    // with backend errors.
    handleError: function(response, scope) {
      scope.loading = false;
      var errorResponse = response.data;
      console.log(errorResponse);
      scope.error = {
        message: errorResponse.message,
        details: []
      };

      for (var k in errorResponse.details) {
        if (errorResponse.details.hasOwnProperty(k)) {
          scope.error.details.push(k + ': ' + errorResponse.details[k]);
        }
      }
    }
  };
  return service;
});