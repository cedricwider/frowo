'use strict';

var woopApp = woopApp || {};

woopApp.service('MeService', ['TokenService', 'UserFactory', '$rootScope', function(tokenService, userFactory, rootScope) {
  var service = {
    me: undefined,

    setMe: function(me) {
      service.me = me;
    },

    loadMe: function() {
      var token = tokenService.getToken();
      userFactory.token(token);
      var that = this;
      userFactory.findMe(that.setMe);
    },

    loadMeAnd: function(succ) {
      var token = tokenService.getToken();
      userFactory.token(token);
      var that = this;
      userFactory.findMe(function(user) {
        console.log('Successfully loaded user: ' + JSON.stringify(user));
        that.setMe(user);
        succ(user);
      });
    },

    destroyMe: function() {
      this.me = null;
    }
  };

  rootScope.$on('logout', function () {
    service.destroyMe();
  });

  return service;
}]);