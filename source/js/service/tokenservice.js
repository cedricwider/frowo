'use strict';

var woopApp = woopApp || {};

woopApp.service('TokenService', ['localStorageService', function (storage) {

  var service = {
    token: null,

    saveToken: function(token) {
      storage.clearAll();
      storage.set('authToken', token);
      this.token = token;
    },

    restoreToken: function() {
      this.token = storage.get('authToken');
    },

    getToken: function() {
      if (this.token === null) {
        this.restoreToken();
      }
      return this.token;
    },

    destroyToken: function () {
      this.token = null;
      storage.clearAll();
    }

  };

  return service;

}]);