'use strict';

var woopApp = woopApp || {};

woopApp.service('GeoLocationService', ['$http', function (http) {
  var MAPS_URL = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=';

  var service = {

    getOneAddress: function(latlong, callback) {
      http.get(MAPS_URL+latlong[0]+','+latlong[1]).success(function(data) {
        var result = null;
        if (data.results && data.results.length > 0) {
          result = data.results[0].formatted_address;
        }
        callback(result);
      });
    }

  };



  return service;
}]);
