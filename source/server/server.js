'use strict';

const
  express = require('express'),
  path = require('path'),
  favicon = require('static-favicon'),
  log = require('npmlog'),
  session = require('cookie-session'),
  cookieParser = require('cookie-parser'),
  passport = require('passport'),
  app = express(),

  proxy = require('./proxy/proxy'),
  auth = require('./auth/auth'),
  signup = require('./auth/register');

/// configuration

app.enable('trust proxy');
app.use(favicon());
app.use(cookieParser());
app.use(session({secret: 'asdflkjh123asdfbayALDhav723lasdjfHLDFGasdlfjkh7SLKJFfh'}));
app.use(passport.initialize());
app.use(passport.session());


/// routes

app.use(express.static(path.join(__dirname + '/../../public')));
app.use('/auth', auth);
app.use('/signup', signup);
app.use('/api', function(req, res) {
  req.url = '/api' + req.url;
  proxy(req, res);
});


/// error handlers

// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;
