/**
 * Created by cedster on 28/09/14.
 */
'use strict';

const
  request = require('request-json'),
  log = require('npmlog'),

  backendHost = process.env.BACKEND_HOST || 'localhost',
  backendPort = process.env.BACKEND_PORT || 9292,
  client = request.newClient('http://' + backendHost + ':' + backendPort);

let OAuthClient = {
  register: function(provider, userKey, userSecret, next) {
    let data = {
      provider: provider,
      key: userKey,
      secret: userSecret
    };
    client.post('/account/signup/' + provider, data, function(err, res, body) {
      if (err) {
        log.error('Error while registering');
        next(err);
      } else {
        log.write('Received server response ' + JSON.stringify(body));
        if (body.success !== true) {
          next(body);
        } else {
          next(null, body);
        }
      }
    });
  },

  login: function(provider, userKey, userSecret, next) {
    client.post('/account/signin/' + provider,
      {
        key: userKey,
        secret: userSecret
      },
      function (err, res, body) {
      if (err) {
        log.error('Error while logging in');
        next(err);
      } else {
        log.write('Received server response ' + JSON.stringify(body));
        if (body.success !== true) {
          next(body);
        } else {
          next(null, body);
        }
      }
    });
  }
};

module.exports = OAuthClient;


