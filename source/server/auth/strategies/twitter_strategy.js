'use strict';
const
  oauth = require('../../restclient/oauthclient'),
  TwitterStrategy = require('passport-twitter').Strategy;

let twitterStrategy = {
  twitterConfig: {
    consumerKey: process.env.TWITTER_API_KEY,
    consumerSecret: process.env.TWITTER_API_SECRET,
    callbackURL: process.env.TWITTER_CALLBACK_URL || 'http://127.0.0.1:3000/auth/twitter/callback'
  },

  createSignin: function() {
    return new TwitterStrategy(
      this.twitterConfig,

      function(token, tokenSecret, profile, done) {
        if (!token || !tokenSecret || !profile) {
          done(null, false, {message: 'Could not authenticate via twitter'});
        }

        oauth.login('twitter', token, tokenSecret, function (err, usr) {
          if (err) {
            done(null, false, {message: 'Error while logging in'});
          } else {
            done(null, usr);
          }
        });
      }
    );
  },

  createSignup: function() {
    return new TwitterStrategy(
      this.twitterConfig,

      function(token, tokenSecret, profile, done) {
        if (!token || !tokenSecret || !profile) {
          done(null, false, {message: 'Could not authenticate via twitter'});
        }

        oauth.register('twitter', token, tokenSecret, function (err, usr) {
          if (err && err.success !== true) {
            done(null, false, {message: 'Error while logging in'});
          } else {
            done(null, usr || err);
          }
        });
      }
    );
  }
};

module.exports = twitterStrategy;