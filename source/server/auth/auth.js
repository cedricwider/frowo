'use strict';

const
  express = require('express'),
  twitterStrategy = require('./strategies/twitter_strategy'),
  passport = require('passport'),
  app = express();

// Passport configuration
passport.use(twitterStrategy.createSignin());

// passport serialization functions
passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

// Application routes
app.get('/twitter', passport.authenticate('twitter'));
app.get('/twitter/callback',
  passport.authenticate('twitter', {
    successRedirect: '/auth/success',
    errorRedirect: '/#/login'
  })
);
app.get('/success', function(req, res){
  res.redirect('/#/authresp/' + req.session.passport.user.authToken);
});

module.exports = app;