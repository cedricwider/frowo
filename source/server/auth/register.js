'use strict';

const
  express = require('express'),
  twitterStrategy = require('./strategies/twitter_strategy'),
  passport = require('passport'),
  app = express();

// Passport configuration
passport.use(twitterStrategy.createSignup());

// passport serialization functions
passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

// Application routes
app.get('/twitter', passport.authenticate('twitter'));
app.get('/twitter/callback',
  passport.authenticate('twitter', {
    successRedirect: '/#/authresp/123',
    errorRedirect: '/#/login'
  }));

module.exports = app;