'use strict';

const
  proxy = require('http-proxy'),
  log = require('npmlog'),

  backendHost = process.env.BACKEND_HOST || 'localhost',
  backendPort = process.env.BACKEND_PORT || 9292,
  httpProxy = proxy.createProxyServer({
    target: 'http://' + backendHost + ':' + backendPort,
    changeOrigin: true
  }),

  doProxy = function(req, res) {
    log.info('Proxy: ' + req.headers.host + req.url + ' --> http://' + backendHost + ':' + backendPort + req.url);

    req.headers.host = backendHost;
    httpProxy.web(req, res, {
      host: backendHost,
      port: backendPort,
      enable: { xforward: true }
    });
  };

module.exports = doProxy;