'use strict';

var proxySnippet = require('grunt-connect-proxy/lib/utils').proxyRequest;
module.exports = function (grunt) {
  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  var appConfig = {
      app: './source',
      dist: './public',
      backendUrl: 'salty-island-6630.herokuapp.com'
  };

  grunt.initConfig({

    // Project settings
    woop: appConfig,

    concurrent: {
      test: ['karma']
    }, 

    // Watches files for changes and runs tasks based on the changed files
    watch: {
      bower: {
        files: ['bower.json'],
        tasks: ['wiredep']
      },
      js: {
        files: ['<%= woop.app %>/js/{,*/}*.js'],
        tasks: ['clean:js', 'newer:jshint:all', 'copy:js'],
        options: {
          livereload: '<%= connect.livereload.options.livereload %>'
        }
      },
      haml: {
        files: ['<%= woop.app %>/templates/**/*.haml'],
        tasks: ['haml', 'wiredep:app']
      },
      jsTest: {
        files: ['test/js/{,*/}*.js'],
        tasks: ['newer:jshint:test', 'karma']
      },
      css: {
        files: ['<%= woop.app %>/styles/{,*/}*.css'],
        tasks: ['copy:css']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      livereload: {
        options: {
          livereload: 35729
        },
        files: [
          '<%= woop.dist %>/{,*/}*.html',
          '.tmp/styles/{,*/}*.css',
          '<%= woop.app %>/js/{,*}*.js',
          '<%= woop.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
        ]
      }
    },


    // The actual grunt server settings
    connect: {
      livereload: {
        options: {
          hostname: 'localhost',
          port: 9000,
          livereload: 35729,
          logger: 'dev',
          open: true
        }
      },
      test: {
        options: {
          port: 9001,
          middleware: function (connect) {
            return [
              connect.static('.tmp'),
              connect.static('test'),
              connect().use(
                '/bower_components',
                connect.static('./bower_components')
              ),
              connect.static(appConfig.dist)
            ];
          }
        }
      },
      dist: {
        proxies: [
          {
            context: '/api',
            host: appConfig.backendUrl,
            headers: {
              'host': appConfig.backendUrl
            },
            https: false,
            changeOrigin: false
          },
          {
            context: '/auth',
            host: appConfig.backendUrl,
            headers: {
              'host': appConfig.backendUrl
            },
            https: false,
            changeOrigin: false
          }
        ],
        options: {
          port: 80,
          hostname: '0.0.0.0',
          middleware: function (connect) {
            return [
              proxySnippet,
              connect.static('.tmp'),
              connect().use(
                '/bower_components',
                connect.static('./bower_components')
              ),
              connect.static(appConfig.dist)
            ];
          }
        }
      }
    },

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: {
        src: [
          'Gruntfile.js',
          '<%= woop.app %>/js/{,*/}*.js'
        ]
      },
      test: {
        options: {
          jshintrc: 'test/.jshintrc'
        },
        src: ['test/js/{,*/}*.js']
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [
          {
            dot: true,
            src: [
              '.tmp',
              '<%= woop.dist %>/{,*/}*',
              '!<%= woop.dist %>/.git*'
            ]
          }
        ]
      },
      js: {
        src: ['<%= woop.dist %>/js/**/*.js', '!<%= woop.dist %>/js/lib/**/*.js']
      },
      server: '.tmp'
    },

    // Add vendor prefixed styles
    autoprefixer: {
      options: {
        browsers: ['last 1 version']
      },
      dist: {
        files: [
          {
            expand: true,
            cwd: '.tmp/styles/',
            src: '{,*/}*.css',
            dest: '.tmp/styles/'
          }
        ]
      }
    },

    // Automatically inject Bower components into the app
    wiredep: {
      options: {
        cwd: '<%= woop.app %>/..'
      },
      app: {
        src: ['<%= woop.dist %>/index.html'],
        ignorePath: /(\.\.\/){1,2}bower_components/,
        fileTypes: {
          html: {
            replace: {
              js: '<script src="/js/lib{{filePath}}"></script>',
              css: '<link rel="stylesheet" href="/styles{{filePath}}" />'
            }
          }
        }
      }
    },

    // Compiles .html.haml files to proper .html files
    haml: {
      compile: {
        files: grunt.file.expandMapping('source/templates/**/*.haml', 'public/', {
          rename: function (base, path) {
            var destPath = base + path.replace(/(?:\.html)?\.haml$/, '.html').replace(/source\/templates\//, '');
            console.log(destPath);
            return destPath;
          }
        }),
        options: {
          language: 'coffee',
          target: 'html'
        }
      }
    },

    // Reads HTML for usemin blocks to enable smart builds that automatically
    // concat, minify and revision files. Creates configurations in memory so
    // additional tasks can operate on them
    useminPrepare: {
      html: '<%= woop.dist %>/index.html',
      options: {
        dest: '<%= woop.dist %>',
        flow: {
          html: {
            steps: {
              js: ['concat', 'uglifyjs'],
              css: ['cssmin']
            },
            post: {}
          }
        }
      }
    },

    // Performs rewrites based on filerev and the useminPrepare configuration
    usemin: {
      html: ['<%= woop.dist %>/{,*/}*.html'],
      css: ['<%= woop.dist %>/styles/{,*/}*.css'],
      options: {
        assetsDirs: ['<%= woop.dist %>', '<%= woop.dist %>/images']
      }
    },

    htmlmin: {
      dist: {
        options: {
          collapseWhitespace: true,
          conservativeCollapse: true,
          collapseBooleanAttributes: true,
          removeCommentsFromCDATA: true,
          removeOptionalTags: true
        },
        files: [
          {
            expand: true,
            cwd: '<%= woop.dist %>',
            src: ['*.html', 'views/{,*/}*.html'],
            dest: '<%= woop.dist %>'
          }
        ]
      }
    },

    // ngmin tries to make the code safe for minification automatically by
    // using the Angular long form for dependency injection. It doesn't work on
    // things like resolve or inject so those have to be done manually.
    ngmin: {
      dist: {
        files: [
          {
            expand: true,
            cwd: '.tmp/concat/scripts',
            src: '*.js',
            dest: '.tmp/concat/scripts'
          }
        ]
      }
    },

    // Replace Google CDN references
    cdnify: {
      dist: {
        html: ['<%= woop.dist %>/*.html']
      }
    },

    // Copies remaining files to places other tasks can use
    copy: {
      dist: {
        files: [
          {
            expand: true,
            dot: true,
            cwd: '<%= woop.app %>',
            dest: '<%= woop.dist %>',
            src: [
              '*.{ico,png,txt}',
              '.htaccess',
              '*.html',
              'images/{,*/}*.{webp}',
              'fonts/*',
              'js/**/*.js'
            ]
          },
          {
            expand: true,
            cwd: '.tmp/images',
            dest: '<%= woop.dist %>/images',
            src: ['generated/*']
          },
          {
            expand: true,
            cwd: './bower_components/',
            src: 'bootstrap-css/css/bootstrap.min.css',
            dest: '<%= woop.dist %>/styles/'
          },
          {
            expand: true,
            cwd: './bower_components/',
            src: 'bootstrap-css/fonts/*',
            dest: '<%= woop.dist %>/styles/'
          }
        ]
      },
      js: {
        files: [
          {
            expand: true,
            cwd: '<%= woop.app %>',
            src: ['**/*.js'],
            dest: '<%= woop.dist %>'
          }
        ]
      },
      css: {
        files: [
          {
            expand: true,
            cwd: '<%= woop.app %>',
            src: ['styles/cedstyles.css'],
            dest: '<%= woop.dist %>'
          },
          {
            expand: true,
            cwd: './bower_components',
            src: ['**/*.css'],
            dest: '<%= woop.dist %>/styles/'
          }
        ]
      },
      dev: {
        files: [
          {
            expand: true,
            cwd: './bower_components/',
            src: ['**/*.js'],
            dest: '<%= woop.dist %>/js/lib/'
          }
        ]
      },
      prod: {
        files: [
          {
            expand: true,
            cwd: './bower_components/',
            src: ['**/*.min.js'],
            dest: '<%= woop.dist %>/js/lib/'
          }
        ]
      },
      styles: {
        expand: true,
        cwd: '<%= woop.app %>/styles',
        dest: '.tmp/styles/',
        src: '{,*/}*.css'
      }
    },

    // Test settings
    karma: {
      unit: {
        configFile: 'test/karma.conf.js',
        singleRun: true
      }
    }
  });

  grunt.registerTask('serve', 'Compile then start a connect web server', function (target) {
      if (target === 'dist') {
        grunt.log.warn('Running serve:dist');
        return grunt.task.run([
          'configureProxies:dist',
          'connect:dist:keepalive']);
      }

      grunt.task.run([
        'clean:server',
        'haml',
        'wiredep',
        'copy:dist',
        'autoprefixer',
        'configureProxies:livereload',
        'connect:livereload',
        'watch'
      ]);
    });

  grunt.registerTask('heroku:', [
    'build'
  ]);
  grunt.registerTask('server', 'DEPRECATED TASK. Use the "serve" task instead', function (target) {
    grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
    grunt.task.run(['serve:' + target]);
  });

  grunt.registerTask('test', [
    'clean:server',
    'concurrent:test',
    'autoprefixer',
    'connect:test',
    'karma'
  ]);

  grunt.registerTask('build', [
    'clean:dist',
    'haml',
    'wiredep',
    'useminPrepare',
    'copy:dist',
    'copy:css',
    'copy:dev',
    'copy:styles'
  ]);

  grunt.registerTask('default', [
    'newer:jshint',
    'test',
    'build'
  ]);

  grunt.registerTask('dev', [
    'build',
    'autoprefixer',
    'watch'
  ]);
};